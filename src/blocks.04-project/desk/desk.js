import {debounce} from 'lodash';

const Selector = '[data-toggle="ellipsis"]';
class Ellipsis {
    constructor(elem) {
        this.block = elem;
        this.text = this.block.innerText;
        this.styles = window.getComputedStyle(this.block);
        this.fontSize = this.styles.getPropertyValue('font-size');
        this.lineHeight = this.styles.getPropertyValue('line-height');
        this.fontWeight = this.styles.getPropertyValue('font-weight');

        // Bind functions;
        this.init = this.init.bind(this);
        this.destroy = this.destroy.bind(this);
        this.calculateDot = this.calculateDot.bind(this);

        this.debounceResize = debounce(this.calculateDot, 2000);

        this.init();
    }

    /**
     * Инициализация
     */
    init() {
        if (!Ellipsis.checkSupportStyle('-webkit-line-clamp', '2')) {
            this.initDot();
            window.addEventListener('resize', this.debounceResize);
        }
    }

    /**
     * Расчет для обрезания текста
     */
    calculateDot() {
        this.block.innerHTML = `<div>${this.text}</div>`;
        let innerElem = this.block.getElementsByTagName('div').item(0);
        innerElem.style.fontSize = this.fontSize;
        innerElem.style.lineHeight = this.lineHeight;
        innerElem.style.fontWeight = this.fontWeight;
        let textLength = this.text.length;


        for (; textLength >= 0 && innerElem.clientHeight > this.block.clientHeight; --textLength) {
            innerElem.innerHTML = this.text.substring(0, textLength) + '...';
        }

        this.block.innerHTML = innerElem.innerText;
    }

    /**
     * Уничтожение расчета
     */
    destroy() {
        window.removeEventListener('resize', this.debounceResize);
    }

    /**
     * Проверка поддержки свойства браузером
     * @param {string} property - свойство
     * @param {string} value - значение свойства
     * @return {boolean} - результат поддержки свойства браузером
     */
    static checkSupportStyle(property, value) {
        try {
            let element = document.createElement('span');
            if (element.style[property] !== undefined) {
                element.style[property] = value;
            } else {
                return false;
            }

            return element.style[property] === value;
        } catch (e) {
            return false;
        }
    }

    static getAllBlocks() {
        return Array.from(document.querySelectorAll(Selector));
    }

    static getAllInstances() {
        return Ellipsis.getAllBlocks().filter(({ins}) => !ins);
    }

    static initAllBlocks() {
        const Blocks = Ellipsis.getAllBlocks();
        Blocks.forEach((block) => {
            if (block.ins) return;
            block.ins = new Ellipsis(block);
        });
    }
}

// Инициашизация;
window.addEventListener('DOMContentLoaded', Ellipsis.initAllBlocks);

// Экспорт;
window.Block = window.Block ? window.Block : {};
window.Block.Ellipsis= Ellipsis;
export default Ellipsis;