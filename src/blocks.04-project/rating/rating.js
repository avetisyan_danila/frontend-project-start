const Rating = function(elem) {
    const inputs = Array.from(elem.querySelectorAll('input'));

    elem.addEventListener('change', ({target}) => {
        // Задизейблить рейтинг
        inputs.forEach((input) => input.disabled = true);
        elem.classList.add('disabled');

        // Заглушка запроса
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(() => {
                const randomRating = Math.floor(Math.random() * 9) + 1; // От 1 до 10
                inputs[randomRating].checked = true;
        });
    });
};

const stars = Array.from(document.querySelectorAll('.rating'));
stars.forEach((ratingEl) => new Rating(ratingEl));