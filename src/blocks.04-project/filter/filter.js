import * as nouislider from 'nouislider';
import {throttle} from 'throttle-debounce';
import Inputmask from 'inputmask';
import 'select2';
import 'nouislider/dist/nouislider.css';

class Range {
    constructor(elem) {
        this.block = elem;
        this.controls = [...this.block.querySelectorAll('input')];
        this.range = this.block.getElementsByClassName('range__slider').item(0);

        this.min = Number(this.range.getAttribute('data-min')) || 0;
        this.max = Number(this.range.getAttribute('data-max')) || 999999;

        this.init();
    }

    init() {
        new Inputmask({
            alias: 'integer',
            showMaskOnHover: false,
            rightAlign: false,
            min: this.min,
            max: this.max,
        }).mask(this.controls[0]);

        new Inputmask({
            alias: 'integer',
            showMaskOnHover: false,
            rightAlign: false,
            min: this.min,
            max: this.max,
        }).mask(this.controls[1]);

        nouislider.create(this.range, {
            animate: true,
            animationDuration: 300,
            connect: true,
            start: [
                this.controls[0].value || this.min,
                this.controls[1].value || this.max,
            ],
            range: {
                min: this.min,
                max: this.max,
            },
            format: {
                to: function(value) {
                    return parseInt(value);
                },
                from: function(value) {
                    return parseInt(value);
                },
            },
        });
        this.range.noUiSlider.on('update', (values, index) => {
            let target = this.controls[index];
            target.value = values[index];
        });

        this.range.noUiSlider.on(
            'change',
            throttle(500, (values, index) => {
                let customEvent = document.createEvent('Event');
                customEvent.initEvent('change', true, false);
                this.controls[index].dispatchEvent(customEvent);
            })
        );

        this.controls.forEach((control, index) => {
            control.addEventListener('change', (e) => {
                this.range.noUiSlider.setHandle(index, e.target.value);
            });
        });
    }
}

class FormControl {
    constructor(elem) {
        this.block = elem;

        switch (this.block.tagName) {
            case 'SELECT':
                this.initSelect();
                break;
            default:
        }
    }

    initSelect() {
        const $block = $(this.block);
        const defaultOptions = {
            width: 'style',
            placeholder: this.block.getAttribute('data-placeholder') || false,
            containerCssClass: 'form-control',
            dropdownCssClass: 'form-control__dropdown',
            dropdownParent: this.block.dataset.parent ? $(this.block.dataset.parent) : $(document.body),
        };

        $block.select2(defaultOptions);
    }
}


[...document.getElementsByClassName('range')].forEach((block) => new Range(block));
[...document.getElementsByClassName('form-control')].forEach((block) => new FormControl(block));

const form = document.querySelector('.filter');
const formSubmitButton = document.querySelector('.submit');
const formResetButton = document.querySelector('.reset');

formSubmitButton.addEventListener('click', (e) => {
    e.preventDefault();
    form.submit();
});

formResetButton.addEventListener('click', (e) => {
    e.preventDefault();
    form.reset();
});
