module.exports = {
    block: 'page',
    title: 'Task 4',
    content: [
        {block: 'container', content: [
            {block: 'inner', mods: {'task': 4}, content: [
                {block: 'navigation', content: [...new Array(6)].map((v, i) => [
                    {elem: 'item', content: [
                        {elem: 'link', attrs: {href: '#'}, content: `Ссылка ${i + 1}`},
                    ]},
                ])},
            ]},
        ]},
    ],
};
