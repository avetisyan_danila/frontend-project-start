module.exports = {
    block: 'page',
    title: 'Task 7',
    content: [
        {cls: 'container', content: [
            {block: 'inner', mods: {'task': 7}, content: [
                {elem: 'title', content: 'Заголовок'},
                {block: 'desk', content: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.'},
            ]},
        ]},
    ],
};
