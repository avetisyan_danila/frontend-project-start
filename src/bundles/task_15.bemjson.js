module.exports = {
    block: 'page',
    title: 'Task 15',
    content: [
        {cls: 'container', content: [
            {block: 'filter', attrs: {action: 'https://echo.htmlacademy.ru/courses', method: 'POST'}, content: [
                {elem: 'header', content: [
                    {elem: 'title', content: 'Поиск по характеристикам'},
                ]},
                {elem: 'body', content: [
                    {elem: 'section', content: [
                        {elem: 'label', content: 'Цена'},
                        {block: 'range', content: [
                            {elem: 'header', content: [
                                {elem: 'slider', attrs: {'data-min': 0, 'data-max': 1000}},
                            ]},
                            {elem: 'body', content: [
                                {block: 'form-control', placeholder: '0'},
                                {block: 'form-control', placeholder: '1000'},
                            ]},
                        ]},
                    ]},
                    {elem: 'section', content: [
                        {elem: 'label', content: 'Бренд'},
                        {block: 'custom-control', cls: 'custom-checkbox', content: [
                            {block: 'custom-control-input', attrs: {type: 'checkbox', id: 'brand-sony'}},
                            {block: 'custom-control-label', attrs: {for: 'brand-sony'}, content: 'Sony'},
                        ]},
                        {block: 'custom-control', cls: 'custom-checkbox', content: [
                            {block: 'custom-control-input', attrs: {type: 'checkbox', id: 'brand-samsung'}},
                            {block: 'custom-control-label', attrs: {for: 'brand-samsung'}, content: 'Samsung'},
                        ]},
                        {block: 'custom-control', cls: 'custom-checkbox', content: [
                            {block: 'custom-control-input', attrs: {type: 'checkbox', id: 'brand-apple'}},
                            {block: 'custom-control-label', attrs: {for: 'brand-apple'}, content: 'Apple'},
                        ]},
                    ]},
                    {elem: 'section', content: [
                        {elem: 'label', content: 'Вид'},
                        {block: 'form-control', attrs: {'name': 'type', 'data-placeholder': 'Выберите вид'}, tag: 'select', content: [
                            {tag: 'option', attrs: {value: ''}, content: ''},
                            'Тип 1',
                            'Тип 2',
                            'Тип 3',
                            'Тип 4',
                            'Тип 5',
                            'Тип 6',
                            'Тип 7',
                            'Тип 8',
                            'Тип 9',
                        ]},
                    ]},
                ]},
                {elem: 'footer', content: [
                    {block: 'btn', cls: 'submit btn-primary', attrs: {type: 'submit'}, content: 'Применить'},
                    {block: 'btn', attrs: {type: 'reset'}, cls: 'reset btn-secondary', content: 'Сбросить'},
                ]},
            ]},
        ]},
    ],
};
