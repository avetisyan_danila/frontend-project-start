let data = [
    {
        name: 'Заголовок',
        desk: 'это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum',
    },
    {
        name: 'Заголовок',
        desk: 'это текст-"рыба", часто используемый в печати и вэб-дизайне. ' +
        'Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. ' +
        'В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.',
    },
];

module.exports = {
    block: 'page',
    title: 'Task 5',
    content: [
        {cls: 'container', content: [
            {cls: 'row', content: data.map((v, i) => [
                {cls: 'col-6 mb-5 px-4', content: [
                    {block: 'card', cls: 'p-3', content: [
                        {block: 'h', size: 4, content: data[i].name},
                        {block: 'shadow-text', content: [
                            {block: 'desk', content: data[i].desk},
                        ]},
                    ]},
                ]},
            ])},
        ]},
    ],
};
