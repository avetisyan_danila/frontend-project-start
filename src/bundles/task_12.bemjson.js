module.exports = {
    block: 'page',
    title: 'Task 12',
    content: [
        {block: 'control-wrapper', content: [
            {elem: 'input', attrs: {id: 'control', type: 'email', placeholder: ' ', autocomplete: 'off'}},
            {elem: 'label', attrs: {for: 'control'}, content: 'Ваше имя'},
        ]},
    ],
};
