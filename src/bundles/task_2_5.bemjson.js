module.exports = {
    block: 'page',
    title: 'Task 2_5',
    content: [
        {block: 'authorization', mods: {'fifth': true}, content: [
            {elem: 'inner', mods: {'task': 2, 'fifth': true}, content: [
                {block: 'form', mods: {'task': 2}, content: [
                    {block: 'h', size: 3, content: 'Авторизация'},
                    {block: 'form-block', content: [
                        {block: 'label', content: [
                            'Email',
                            {block: 'form-control', attrs: {type: 'email'}},
                        ]},
                    ]},
                    {block: 'form-block', content: [
                        {block: 'label', content: [
                            'Password',
                            {block: 'form-control', attrs: {type: 'password'}},
                        ]},
                    ]},
                    {block: 'btn', content: 'Log In'},
                ]},
            ]},
        ]},
    ],
};
