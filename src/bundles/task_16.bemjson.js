module.exports = {
    block: 'page',
    title: 'Task 16',
    content: [
        {block: 'related-list', cls: 'container', content: [
            {cls: 'row', content: [
                {cls: 'col-4', content: [
                    {block: 'form-control', attrs: {'id': 'car-mark', 'name': 'car-mark', 'data-placeholder': 'Выберите марку', 'data-target': '#car-model'}, tag: 'select'},
                ]},
                {cls: 'col-4', content: [
                    {block: 'form-control', attrs: {'id': 'car-model', 'name': 'models', 'data-placeholder': 'Выберите модель', 'data-target': '#car-year'}, tag: 'select'},
                ]},
                {cls: 'col-4', content: [
                    {block: 'form-control', attrs: {'id': 'car-year', 'name': 'years', 'data-placeholder': 'Выберите год'}, tag: 'select'},
                ]},
            ]},
        ]},
    ],
};
