module.exports = {
    block: 'page',
    title: 'Task 8',
    content: [
        {block: 'form', mods: {'task': 8}, content: [
            {elem: 'input'},
            {elem: 'icon'},
        ]},
    ],
};
