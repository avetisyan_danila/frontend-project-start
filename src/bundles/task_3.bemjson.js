module.exports = {
    block: 'page',
    title: 'Task 3',
    content: [
        {block: 'rating', cls: 'mx-5 py-2', content: [...new Array(10)].map((v, i) => [
            {elem: 'input', attrs: {type: 'radio', id: `rating-1${10 - i}`, name: 'rating', value: 10 - i}},
            {elem: 'label', attrs: {title: `${(10 - i) / 2} из 5`, for: `rating-1${10 - i}`}},
        ])},
        {block: 'rating', cls: 'mx-5 py-2', content: [...new Array(10)].map((v, i) => [
            {elem: 'input', attrs: {type: 'radio', id: `rating-2${10 - i}`, name: 'rating', value: 10 - i}},
            {elem: 'label', attrs: {title: `${(10 - i) / 2} из 5`, for: `rating-2${10 - i}`}},
        ])},
        {block: 'rating', cls: 'mx-5 py-2', content: [...new Array(10)].map((v, i) => [
            {elem: 'input', attrs: {type: 'radio', id: `rating-3${10 - i}`, name: 'rating', value: 10 - i}},
            {elem: 'label', attrs: {title: `${(10 - i) / 2} из 5`, for: `rating-3${10 - i}`}},
        ])},
    ],
};
