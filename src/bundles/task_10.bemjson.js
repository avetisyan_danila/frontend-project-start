module.exports = {
    block: 'page',
    title: 'Task 10',
    content: [
        {block: 'rhomb', mods: {'task': 10}, content: [
            {elem: 'image', attrs: {src: 'https://via.placeholder.com/300x300'}},
        ]},
    ],
};
