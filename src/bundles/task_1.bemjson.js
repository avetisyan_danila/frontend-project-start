 let data = [
     {
         name: 'Название',
     },
     {
         name: 'Длинное название товара',
         desk: 'Длинное описание товара, может даже в пару абзацев текста',
     },
     {
         name: 'Название',
     },
     {
         name: 'Название в пару строк',
     },
     {
         name: 'Название',
     },
     {
         name: 'Длинное название товара',
         desk: 'Длинное описание товара, может даже в пару абзацев текста',
     },
 ];

module.exports = {
    block: 'page',
    title: 'Task 1',
    content: [
        {cls: 'container', content: [
            {cls: 'row', content: data.map((v, i) => [
                {cls: 'col-4 mb-5', content: [
                    {block: 'card', cls: 'p-1', content: [
                        {block: 'image', mods: {size: '350x150'}},
                        {block: 'h', size: 4, content: data[i].name},
                        {block: 'desk', content: data[i].desk},
                        {block: 'btn', content: 'Купить'},
                    ]},
                ]},
            ])},
        ]},
    ],
};
