module.exports = {
    block: 'page',
    title: 'Task 9',
    content: [
        {block: 'wrapper', mods: {'task': 9}, content: [
            {block: 'table', content: [
                {elem: 'thead', content: [
                    {elem: 'tr', content: [...new Array(5)].map((v, i) => [
                        {elem: 'th', content: [
                            {content: `Заголовок ${i + 1}`},
                        ]},
                    ])},
                ]},
                {elem: 'tbody', content: [...new Array(15)].map((v, i) => [
                    {elem: 'tr', content: [...new Array(5)].map((v, i) => [
                        {elem: 'td', content: i + 1},
                    ])},
                ])},
            ]},
        ]},
    ],
};
