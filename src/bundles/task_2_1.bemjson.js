module.exports = {
    block: 'page',
    title: 'Task 2_1',
    content: [
        {block: 'authorization', cls: 'd-flex justify-content-center align-items-center', content: [
            {elem: 'inner', mods: {'task': 2}, content: [
                {block: 'form', mods: {'task': 2}, content: [
                    {block: 'h', size: 3, content: 'Авторизация'},
                    {block: 'form-block', content: [
                        {block: 'label', content: [
                            'Email',
                            {block: 'form-control', attrs: {type: 'email'}},
                        ]},
                    ]},
                    {block: 'form-block', content: [
                        {block: 'label', content: [
                            'Password',
                            {block: 'form-control', attrs: {type: 'password'}},
                        ]},
                    ]},
                    {block: 'btn', content: 'Log In'},
                ]},
            ]},
        ]},
    ],
};
