module.exports = {
    block: 'page',
    title: 'Task 11',
    content: [
        {cls: 'container', content: [
            {cls: 'row justify-content-center', content: [
                {cls: 'col-4', content: [
                    {block: 'card', content: [
                        {block: 'sale', content: [
                            {elem: 'icon', content: 'sale'},
                        ]},
                        {block: 'image', mods: {size: '350x150'}},
                        {block: 'card-body', content: [
                            {block: 'h', size: 3, content: 'Заголовок'},
                            {block: 'desk', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ],
};
