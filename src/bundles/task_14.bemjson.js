module.exports = {
    block: 'page',
    title: 'Task 14',
    content: [
        {block: 'wrapper', mods: {'task': 14}, cls: 'container', content: [
            {block: 'header', content: 'Header'},
            {block: 'main', content: [
                {block: 'sidebar', content: 'Sidebar 1'},
                {block: 'section', content: [
                    {block: 'image', mods: {size: '150x150'}},
                    {block: 'desk', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'},
                    {block: 'desk', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'},
                ]},
                {block: 'sidebar', content: 'Sidebar 2'},
            ]},
            {block: 'footer', content: 'Footer'},
        ]},
    ],
};
